#!/bin/bash

zkespre=""
zksvclog="/run/zk_svc.log"

get_cur_uptime()
{
   local uptm=`cat /proc/uptime | cut -f 1 -d" "`
   echo $uptm
}

# Get the time when this script was invoked
tm=`date +%s.%N`
uptm="$(get_cur_uptime)"
echo "Start early zymkey svc at ${tm} (uptime=${uptm})" >> ${zksvclog}

get_cryptrfs_svc_time()
{
   local svcname="$1"

   local sttmusstr=`systemctl show ${svcname} --property=ActiveEnterTimestampMonotonic`
   local sttmus=`echo ${sttmusstr} | cut -f2 -d"="`
   local sttm=`awk -v "sttmus=${sttmus}" "BEGIN {print sttmus/1000000}"`
   local exptm=`awk -v "sttm=$sttm" "BEGIN {print sttm + 11}"`
   local uptm="$(get_cur_uptime)"
   local dlytm=`awk "BEGIN {if (${uptm} < ${exptm}) print ${exptm} - ${uptm}; else print 0;}"`
   echo ${dlytm}
}

d1="$(get_cryptrfs_svc_time dev-mapper-cryptrfs.device)"
d2="$(get_cryptrfs_svc_time systemd-cryptsetup@cryptrfs.service)"
dlytm=`awk -v n1="${d1}" -v n2="${d2}" 'BEGIN {printf "%f" (n1 > n2) ? n1 : n2}'`
echo "dm=${d1} cs=${d2} dlytm=${dlytm}" >> ${zksvclog}
sleep ${dlytm}

uptm="$(get_cur_uptime)"
touch /run/zk_svc_start
echo "${zkespre}start time = ${tm} (uptime=${uptm})" >> ${zksvclog}
retry=0
while [ $retry -lt 5 ]
do
   uptm="$(get_cur_uptime)"
   zkbootrtc &> /run/zkbootrtc.status
   status=`cat /run/zkbootrtc.status`
   if [[ $status == "" ]]
   then
      echo "${zkespre}no status returned (uptime=${uptm})" >> ${zksvclog}
   else
      echo "${zkespre}${status} (uptime=${uptm})" &>> ${zksvclog}
      if [[ $status != *"Segfault"* ]] && [[ $status != *"ERROR"* ]] && [[ $status != *"fail"* ]] && [[ $status != *"could not open session"* ]] && [[ $status != *"no response received"* ]]
      then
         break
      fi
   fi
   sleep 10
   let "retry++"
   echo "${zkespre}retry ${retry}..." >> ${zksvclog}
done
if [ $retry -ge 5 ]
then
   echo "${zkespre}FAILED" >> ${zksvclog}
fi
tm=`date +%s.%N`
uptm="$(get_cur_uptime)"
touch /run/zk_svc_complete
echo "${zkespre}complete time = ${tm} (uptime=${uptm})" >> ${zksvclog}

exit 0
